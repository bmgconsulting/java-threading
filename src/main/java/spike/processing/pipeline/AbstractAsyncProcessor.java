package spike.processing.pipeline;

import java.util.concurrent.ExecutorService;

/**
 *
 */
public abstract class AbstractAsyncProcessor<In, Out> extends AbstractProcessor<In, Out> {

    private final ExecutorService executor;

    public AbstractAsyncProcessor(ExecutorService executor) {
        super();
        this.executor = executor;
    }

    @Override
    public void process(In event) throws Exception {
        executor.submit(new ProcessTask(event));
    }

    /**
     * Encapsulates the processing of a given input event in an asynchronous fashion
     */
    private final class ProcessTask implements Runnable {

        private final In event;

        public ProcessTask(In event) {
            this.event = event;
        }

        @Override
        public void run() {
            try{
                final Out out = processImpl(event);                 // calculate the output

                for (Processor<Out, ?> consumer : consumers) {      // pass the output to each consumer
                    consumer.process(out);
                }
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
