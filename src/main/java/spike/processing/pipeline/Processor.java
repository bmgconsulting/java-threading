package spike.processing.pipeline;

/**
 * A simple processing unit designed to be part of a pipeline
 * @param <In> the input type
 * @param <Out> a marker for the output type
 */
public interface Processor<In, Out> {

    void process(In event) throws Exception;

}
