package spike.processing.pipeline;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

/**
 *
 */
public abstract class AbstractProcessor<In, Out> implements Processor<In, Out> {

    protected final List<Processor<Out, ?>> consumers;

    public AbstractProcessor() {
        this.consumers = new CopyOnWriteArrayList<Processor<Out, ?>>();     // thread safe means of tracking consumers
    }

    /**
     * Wire up against other processors
     *
     * @param consumer the consumer we will be feeding
     * @param <T> the output type of the consumer we will be feeding
     * @return the process you just wired up to allowing for chaining
     */
    public <T> AbstractProcessor<Out, T> feeds(AbstractProcessor<Out, T> consumer) {
        consumers.add(consumer);
        return consumer;
    }

    @Override
    public void process(In event) throws Exception {
        try{
            final Out out = processImpl(event);                 // calculate the output

            for (Processor<Out, ?> consumer : consumers) {      // pass the output to each consumer
                consumer.process(out);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Actual implementation of whatever this processor does
     *
     * @param event
     * @return
     * @throws Exception
     */
    protected abstract Out processImpl(In event) throws Exception;

}
