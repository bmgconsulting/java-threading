package spike.processing.pipeline;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This is a simple demonstration of a pipeline pattern which allows for multiple stages of computationally intensive
 * processing that can be backed by a single executor.
 */
public class QuickTest {

    public static void main(String[] args) throws Exception {

        final ExecutorService executor = Executors.newFixedThreadPool(4);       // executor to be used by all stages

        final Doubler doubler = new Doubler(executor);                          // create some processing stages
        final RandomAdder randomAdder = new RandomAdder(executor);
        final Logger logger = new Logger();

        doubler.feeds(randomAdder).feeds(logger);                               // wire them up to each other

        // feed some input to the first stage
        for(int i=0; i<1000; i++){
            doubler.process(i);
        }

    }

    public static final class Doubler extends AbstractAsyncProcessor<Integer, Double> {

        public Doubler(ExecutorService executor) {
            super(executor);
        }

        @Override
        protected Double processImpl(Integer event) throws Exception {
            return event * 2.0;
        }

    }

    public static final class RandomAdder extends AbstractAsyncProcessor<Double, Double> {

        private final Random random = new Random();

        public RandomAdder(ExecutorService executor) {
            super(executor);
        }

        @Override
        protected Double processImpl(Double event) throws Exception {
            return event + random.nextInt(100);
        }
    }

    /**
     * Re-uses the dispatching thread
     */
    public static final class Logger extends AbstractProcessor<Double, Void> {

        @Override
        protected Void processImpl(Double event) throws Exception {
            System.out.println("Pipeline output: " + event);
            return null;
        }

    }
}
