package spike.processing.simple;


public interface Operation<T> {

    enum Type {
        PERSIST, RENAME
    }

    Type type();

}
