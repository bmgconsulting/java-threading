package spike.processing.simple;

import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class WorkQueue {

    private ExecutorService executor;

    public WorkQueue() {
        this.executor = Executors.newFixedThreadPool(1);
    }

    @SuppressWarnings("unchecked")
    public <R, T extends Operation<R>> Future<R> submit(T op) {

        final Operation.Type type = op.type();

        switch (type){
            case PERSIST:
                return (Future<R>) executor.submit(new Persist());

            case RENAME:
                return (Future<R>) executor.submit(new Rename());

            default:
                throw new RuntimeException("Unexpected operation type: " + type);
        }

    }

    private static final class Persist implements Callable<UUID> {

        @Override
        public UUID call() throws Exception {
            return UUID.randomUUID();
        }

    }

    private static final class Rename implements Callable<String> {

        @Override
        public String call() throws Exception {
            return "File-" + System.currentTimeMillis();
        }

    }

}
