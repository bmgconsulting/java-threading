package spike.processing.simple;

import java.util.UUID;

/**
 * Created by bmcgee on 29/01/2016.
 */
public class PersistOperation implements Operation<UUID> {


    @Override
    public Type type() {
        return Type.PERSIST;
    }

}
