package spike.processing.simple;

/**
 * Created by bmcgee on 29/01/2016.
 */
public class RenameOperation implements Operation<String> {

    @Override
    public Type type() {
        return Type.RENAME;
    }

}
