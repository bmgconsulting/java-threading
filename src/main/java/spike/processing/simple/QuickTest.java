package spike.processing.simple;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by bmcgee on 29/01/2016.
 */
public class QuickTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {

        final WorkQueue workQueue = new WorkQueue();
        final List<Future<?>> futures = new ArrayList<Future<?>>();

        for (int i=0; i<1000; i++){

            final boolean renameFile = (i % 10 == 0);

            if(renameFile) {
                // typed future result to let you know when the file has been renamed and what to
                final Future<String> rename = workQueue.submit(new RenameOperation());
                futures.add(rename);
            } else {
                // typed future result to let you know when an entry has been persisted
                final Future<UUID> submit = workQueue.submit(new PersistOperation());
                futures.add(submit);
            }
        }

        for (Future<?> future : futures) {
            Object result = future.get(10, TimeUnit.SECONDS);
            System.out.println("Result of operation: " + result);
        }

        System.exit(0);
    }

}
