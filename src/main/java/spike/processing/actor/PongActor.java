package spike.processing.actor;

/**
 * Created by bmcgee on 31/01/2016.
 */
public class PongActor extends AbstractActor {

    @Override
    protected void receiveImpl(Object message) throws Exception {
        if(message.equals("ping")){
            System.out.println("Received ping. Responding with pong");
            Thread.sleep(1000);
            reply("pong");
        }
    }
}
