package spike.processing.actor;


import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Simple actor pattern implementation with inspiration from Akka. Intended to show the basic principles of how
 * to implement the actor pattern, not intended as a robust solution.
 */
public abstract class AbstractActor {

    private final Queue<ProcessTask> mailbox = new ConcurrentLinkedQueue<ProcessTask>();

    private final AtomicBoolean running = new AtomicBoolean();  // used to ensure only one thread at a time is executing

    private ActorSystem actorSystem;
    private String name;

    protected AbstractActor sender;

    public void setActorSystem(ActorSystem actorSystem) {
        this.actorSystem = actorSystem;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected AbstractActor actorFor(String name){
        return actorSystem.actorFor(name);
    }

    public void receive(String sender, Object message) {    // receive and try to process a message
        mailbox.add(new ProcessTask(sender, message));
        tryExecuting();
    }

    protected void sendTo(String name, Object message){     // utility for sending to another actor
        actorFor(name).receive(getName(), message);
    }

    protected void reply(Object message){                   // utility for responding to an actor
        sender.receive(getName(), message);
    }

    private void tryExecuting(){
        if(running.compareAndSet(false, true)){             // try to acquire the running lock and schedule the processing
            final ProcessTask task = mailbox.poll();        // of the next message
            actorSystem.getExecutor().execute(task);
        }
    }

    protected void onStart(){
        // do nothing by default
    }

    protected abstract void receiveImpl(Object message) throws Exception;

    /**
     * All actor processing is done within a process task. It is used to encapsulate the input to be applied to the
     * actor receiveImpl method.
     */
    private final class ProcessTask implements Runnable {

        private final String sender;
        private final Object message;

        public ProcessTask(String sender, Object message) {
            this.sender = sender;
            this.message = message;
        }

        @Override
        public void run() {

            AbstractActor.this.sender = actorFor(sender);   // set the sending actor in the main actor class

            try {
                receiveImpl(message);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                running.set(false);                         // release the running lock

                if(!mailbox.isEmpty()){                     // if mailbox is not empty immediately schedule the processing
                    tryExecuting();                         // of the next message
                }
            }
        }
    }

}
