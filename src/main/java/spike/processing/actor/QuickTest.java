package spike.processing.actor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by bmcgee on 31/01/2016.
 */
public class QuickTest {

    public static void main(String[] args) {

        // this can run with 1 thread or 100. You can have thousands of actors sharing a single thread if you want.
        final ExecutorService executor = Executors.newFixedThreadPool(1);
        final ActorSystem actorSystem = new ActorSystem(executor);

        actorSystem.register("pongActor", new PongActor());
        actorSystem.register("pingActor", new PingActor());

    }

}
