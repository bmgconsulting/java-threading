package spike.processing.actor;

/**
 * Created by bmcgee on 31/01/2016.
 */
public class PingActor extends AbstractActor {

    @Override
    protected void onStart() {
        sendTo("pongActor", "ping");        // initialise the back and forth with a single ping to start
    }

    @Override
    protected void receiveImpl(Object message) throws Exception {
        if(message.equals("pong")){
            System.out.println("Received pong. Responding with ping");
            reply("ping");
        }
    }
}
