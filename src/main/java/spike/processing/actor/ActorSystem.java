package spike.processing.actor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

/**
 * Created by bmcgee on 31/01/2016.
 */
public class ActorSystem {

    private final ExecutorService executor;
    private final Map<String, AbstractActor> registry = new ConcurrentHashMap<String, AbstractActor>();

    public ActorSystem(ExecutorService executor) {
        this.executor = executor;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public void register(String name, AbstractActor actor){
        registry.put(name, actor);
        actor.setActorSystem(this);
        actor.setName(name);
        actor.onStart();
    }

    public AbstractActor actorFor(String name){
        final AbstractActor abstractActor = registry.get(name);
        if(abstractActor == null){
            throw new RuntimeException("AbstractActor not found: " + name);
        }
        return abstractActor;
    }

}
