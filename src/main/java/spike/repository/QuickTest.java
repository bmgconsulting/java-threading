package spike.repository;

import java.util.concurrent.ExecutionException;

/**
 * Simple pattern for offline capabilities. Note any methods which modify the repository throw are not allowed when offline.
 * To allow for offline modification an Event Sourcing approach is needed with a reconciliation upon reconnection. If you simply
 * want to allow access for data already retrieved this pattern will suffice.
 */
public class QuickTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        // for this test represents a remote source
        final InMemoryUserRepository remoteUserRepository = new InMemoryUserRepository();

        // repository to be used by application code
        final OfflineCapableRepository<String, UserModel> userRepository = new OfflineCapableRepository<String, UserModel>(remoteUserRepository, 10000);

        // create some users

        final UserModel bob = userRepository.create(
                new UserModel()
                    .setEmail("bob@email.com")
                    .setFirstName("Bob")
                    .setLastName("Monkhouse")
        ).get();

        final UserModel jimi = userRepository.create(
                new UserModel()
                        .setEmail("jimi@email.com")
                        .setFirstName("Jimi")
                        .setLastName("Hendrix")
        ).get();

        final UserModel malcolm = userRepository.create(
                new UserModel()
                        .setEmail("malcolm@email.com")
                        .setFirstName("Malcolm")
                        .setLastName("Reynolds")
        ).get();

        // tell the repository to behave as if offline. A more robust solution would have some form of circuit breaker
        // logic which determines when to switch to offline mode based on exceptions/timeouts returned from the remote
        // source or application wide logic

        userRepository.setOffline(true);
        remoteUserRepository.setThrowErrors(true);      // have our fake remote source throw errors so we'll know if it's being used

        // retrieve the entries we put in

        final UserModel offlineBob = userRepository.findById("bob@email.com").get();
        final UserModel offlineJimi = userRepository.findById("jimi@email.com").get();
        final UserModel offlineMalcolm = userRepository.findById("malcolm@email.com").get();

        System.out.println("Does bob match: " + bob.equals(offlineBob));
        System.out.println("Does jimi match: " + jimi.equals(offlineJimi));
        System.out.println("Does malcolm match: " + malcolm.equals(offlineMalcolm));
    }

}
