package spike.repository;

import spike.cache.SettableFuture;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;

/**
 * Created by bmcgee on 31/01/2016.
 */
public class InMemoryRepository<Id, M extends Model<Id, M>> implements Repository<Id, M> {

    private final ConcurrentMap<Id, M> map = new ConcurrentHashMap<Id, M>();

    @Override
    public Future<M> create(M model) {
        // TODO how to throw an error on duplicate key
        map.putIfAbsent(model.getId(), model);
        return new SettableFuture<M>(model);
    }

    @Override
    public Future<M> findById(Id id) {
        final M model = map.get(id);
        return new SettableFuture<M>(model);
    }

    @Override
    public Future<M> updateById(M value) {
        map.put(value.getId(), value);
        return new SettableFuture<M>(value);
    }

    @Override
    public Future<M> deleteById(Id id) {
        final M removed = map.remove(id);
        return new SettableFuture<M>(removed);
    }

    @Override
    public Future<M> delete(M model) {
        map.remove(model.getId(), model);
        return new SettableFuture<M>(model);
    }
}
