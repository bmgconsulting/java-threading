package spike.repository;

import spike.cache.SettableFuture;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class OfflineCapableRepository<Id, M extends Model<Id, M>> implements Repository<Id, M> {

    private final Repository<Id, M> delegate;
    private final InMemoryRepository<Id, M> offlineRepository;
    private final long timeoutMs;

    private volatile boolean isOffline = false;                     // used to switch behaviour

    public OfflineCapableRepository(Repository<Id, M> delegate, long timeoutMs) {
        this.delegate = delegate;
        this.timeoutMs = timeoutMs;
        this.offlineRepository = new InMemoryRepository<Id, M>();   // could be json or whatever else
    }

    public void setOffline(boolean isOffline){
        this.isOffline = isOffline;
    }

    @Override
    public Future<M> create(M model) {
        if(isOffline) throw new RuntimeException("Operation not possible whilst offline");

        Future<M> create = delegate.create(model);

        try {
            final M created = create.get(timeoutMs, TimeUnit.MILLISECONDS);
            create = offlineRepository.create(created);
        } catch(Exception ex){
            create = new SettableFuture<M>(ex);
        }
        return create;
    }

    @Override
    public Future<M> findById(Id id) {
        Future<M> result;

        if(isOffline){
            result = offlineRepository.findById(id);
        } else {
            final Future<M> findById = delegate.findById(id);
            try {
                final M model = findById.get(timeoutMs, TimeUnit.MILLISECONDS);
                result = offlineRepository.updateById(model);
            } catch (Exception ex){
                result = new SettableFuture<M>(ex);
            }
        }

        return result;
    }

    @Override
    public Future<M> updateById(M value) {
        if(isOffline) throw new RuntimeException("Operation not possible whilst offline");

        Future<M> create = delegate.create(value);
        try {
            final M model = create.get(timeoutMs, TimeUnit.MILLISECONDS);
            create = offlineRepository.updateById(model);
        } catch (Exception ex){
            create = new SettableFuture<M>(ex);
        }

        return create;
    }

    @Override
    public Future<M> deleteById(Id id) {
        if(isOffline) throw new RuntimeException("Operation not possible whilst offline");

        Future<M> delete = delegate.deleteById(id);

        try {
            final M model = delete.get(timeoutMs, TimeUnit.MILLISECONDS);
            delete = offlineRepository.deleteById(id);
        } catch (Exception ex){
            delete = new SettableFuture<M>(ex);
        }

        return delete;
    }

    @Override
    public Future<M> delete(M model) {
        return deleteById(model.getId());
    }
}
