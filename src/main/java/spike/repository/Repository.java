package spike.repository;

import java.util.concurrent.Future;

/**
 *
 */
public interface Repository<Id, M extends Model<Id, M>> {

    Future<M> create(M model);

    Future<M> findById(Id id);

    Future<M> updateById(M value);

    Future<M> deleteById(Id id);

    Future<M> delete(M model);

}
