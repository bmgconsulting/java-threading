package spike.repository;

/**
 *
 */
public interface Model<K, V extends Model<K, V>> {

    K getId();

}
