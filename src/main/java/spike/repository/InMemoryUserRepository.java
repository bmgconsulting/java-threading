package spike.repository;

import java.util.concurrent.Future;

/**
 * Created by bmcgee on 01/02/2016.
 */
public class InMemoryUserRepository implements Repository<String, UserModel> {

    private final InMemoryRepository<String, UserModel> delegate;
    private volatile boolean throwErrors;

    public InMemoryUserRepository() {
        if(throwErrors) throw new RuntimeException("I'm broken");
        this.delegate = new InMemoryRepository<String, UserModel>();
    }

    public void setThrowErrors(boolean throwErrors) {
        this.throwErrors = throwErrors;
    }

    @Override
    public Future<UserModel> create(UserModel model) {
        if(throwErrors) throw new RuntimeException("I'm broken");
        return delegate.create(model);
    }

    @Override
    public Future<UserModel> findById(String id) {
        if(throwErrors) throw new RuntimeException("I'm broken");
        return delegate.findById(id);
    }

    @Override
    public Future<UserModel> updateById(UserModel value) {
        if(throwErrors) throw new RuntimeException("I'm broken");
        return delegate.updateById(value);
    }

    @Override
    public Future<UserModel> deleteById(String id) {
        if(throwErrors) throw new RuntimeException("I'm broken");
        return delegate.deleteById(id);
    }

    @Override
    public Future<UserModel> delete(UserModel model) {
        return delegate.delete(model);
    }
}
