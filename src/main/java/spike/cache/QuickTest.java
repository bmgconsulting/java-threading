package spike.cache;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class QuickTest {

    private static final String[] KEYS = new String[]{"foo", "bar", "baz"};

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        final Cache.Store<String, Long> store = new InMemoryCacheStore<String, Long>();
        final Cache<String, Long> cache = new SimpleCache<String, Long>(store);

        final ExecutorService executors = Executors.newCachedThreadPool();

        final List<Future<Void>> futures = new ArrayList<Future<Void>>();

        for(int i=0; i<5; i++){
            final Future<Void> writeFuture = executors.submit(new Writer(cache));
            final Future<Void> readFuture = executors.submit(new Reader(cache));

            futures.add(writeFuture);
            futures.add(readFuture);
        }

        // Wait for all the futures to finish
        for (Future<Void> future : futures) {
            future.get();
        }

        System.exit(0);
    }

    private static final class Writer implements Callable<Void> {

        private final Cache<String, Long> cache;
        private final Random random;

        public Writer(Cache<String, Long> cache) {
            this.cache = cache;
            this.random = new Random();
        }

        @Override
        public Void call() throws Exception {

            for(int i=0; i<1000; i++){
                final int idx = random.nextInt(KEYS.length);
                final String key = KEYS[idx];                   // select a random key
                final boolean remove = random.nextBoolean();    // determine if we write or remove

                if(remove){
                    System.out.println(String.format("Removing key: %s", key));
                    cache.remove(key);
                } else {
                    final long value = System.currentTimeMillis();
                    System.out.println(String.format("Writing key: %s, value: %d", key, value));
                    cache.put(key, value); // write time in to the cache
                }

                Thread.sleep(random.nextInt(50));               // sleep up to 50 ms
            }

            return null;
        }
    }

    private static final class Reader implements Callable<Void> {

        private final Cache<String, Long> cache;
        private final Random random;

        public Reader(Cache<String, Long> cache) {
            this.cache = cache;
            this.random = new Random();
        }

        @Override
        public Void call() throws Exception {

            for(int i=0; i<1000; i++){
                final int idx = random.nextInt(KEYS.length);
                final String key = KEYS[idx];                   // select a random key
                final Long value = cache.get(key);
                System.out.println(String.format("Reading key: %s, value: %s", key, value));
                Thread.sleep(random.nextInt(50));               // sleep up to 50 ms
            }

            return null;
        }
    }
}
