package spike.cache;

import java.util.concurrent.*;

/**
 * Created by bmcgee on 30/01/2016.
 */
public class InMemoryCacheStore<K, V> implements Cache.Store<K, V> {

    private final ConcurrentMap<K, V> map;

    public InMemoryCacheStore() {
        this.map = new ConcurrentHashMap<K, V>();
    }

    @Override
    public Future<V> read(final K key) throws ExecutionException, InterruptedException {
        return new SettableFuture<V>(map.get(key));
    }

    @Override
    public Future<V> write(K key, V value) {
        return new SettableFuture<V>(map.put(key, value));
    }

    @Override
    public Future<V> remove(K key) {
        return new SettableFuture<V>(map.remove(key));
    }
}
