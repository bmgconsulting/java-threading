package spike.cache;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class SimpleCache<K, V> implements Cache<K, V> {

    private final Store<K, V> store;
    private final ConcurrentMap<K, Future<V>> map;

    public SimpleCache(Store<K, V> store) {
        this.store = store;
        this.map = new ConcurrentHashMap<K, Future<V>>();
    }

    @Override
    public V get(K key) throws ExecutionException, InterruptedException {

        Future<V> entry = map.get(key);                     // look for a cache entry

        if(entry == null){
            entry = store.read(key);                        // possible under high load to do multiple reads to the store
            map.putIfAbsent(key, entry);                    // only do a read if the cache has no entry. This is thread safe
        }

        return entry.get();                                 // wait and return the new value
    }

    @Override
    public V put(K key, V value) throws ExecutionException, InterruptedException {
        final Future<V> write = store.write(key, value);    // write to the store
        final Future<V> entry = map.put(key, write);        // update the cache entry
        return write.get();
    }

    @Override
    public V remove(K key) throws ExecutionException, InterruptedException {
        final Future<V> entry = map.remove(key);            // remove from the cache first

        if(entry == null){
            return null;
        }

        final Future<V> removed = store.remove(key);        // remove from the store if non null
        return removed.get();                               // return the removed value
    }
}
