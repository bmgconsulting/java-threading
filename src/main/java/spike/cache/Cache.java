package spike.cache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public interface Cache<K, V> {

    V get(K key) throws ExecutionException, InterruptedException;

    V put(K key, V value) throws ExecutionException, InterruptedException;

    V remove(K key) throws ExecutionException, InterruptedException;

    interface Store<K, V> {

        Future<V> read(K key) throws ExecutionException, InterruptedException;

        Future<V> write(K key, V value);

        Future<V> remove(K key);

    }

}
