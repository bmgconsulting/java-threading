package spike.cache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Simple version of what guava has
 */
public class SettableFuture<T> implements Future<T> {

    private final Throwable throwable;
    private final AtomicReference<T> value;

    public SettableFuture(Throwable throwable) {
        this.throwable = throwable;
        this.value = null;
    }

    public SettableFuture(T value) {
        this.value = new AtomicReference<T>(value);
        this.throwable = null;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return true;
    }

    @Override
    public T get() throws InterruptedException, ExecutionException {
        if(throwable != null) throw new ExecutionException(throwable);
        else return value.get();
    }

    @Override
    public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return get();
    }

}
